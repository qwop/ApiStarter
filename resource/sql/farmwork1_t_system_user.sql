CREATE TABLE farmwork1.t_system_user
(
    uuid varchar(40) PRIMARY KEY NOT NULL,
    in_use varchar(20),
    mobile varchar(20),
    password varchar(40),
    username varchar(40),
    create_date datetime,
    delete_date datetime,
    is_delete varchar(40),
    update_date datetime
);
INSERT INTO farmwork1.t_system_user (uuid, in_use, mobile, password, username, create_date, delete_date, is_delete, update_date) VALUES ('8ac4d16064b5313c0164b532e1e1000a', '1', '15238282572', 'e10adc3949ba59abbe56e057f20f883e', 'admin3', null, null, '1', null);
INSERT INTO farmwork1.t_system_user (uuid, in_use, mobile, password, username, create_date, delete_date, is_delete, update_date) VALUES ('8ac4d16064b5313c0164b532bd110009', '0', '15238282572', 'e10adc3949ba59abbe56e057f20f883e', 'admin1', null, null, null, null);
INSERT INTO farmwork1.t_system_user (uuid, in_use, mobile, password, username, create_date, delete_date, is_delete, update_date) VALUES ('4028803364b2ce370164b2de36f1000d', '1', '15238282572', 'e10adc3949ba59abbe56e057f20f883e', 'admin2', null, null, null, null);
INSERT INTO farmwork1.t_system_user (uuid, in_use, mobile, password, username, create_date, delete_date, is_delete, update_date) VALUES ('4028803364b7ca320164b7cf2bfc000d', '0', '15238282572', 'e10adc3949ba59abbe56e057f20f883e', 'qqqq', null, null, null, null);
INSERT INTO farmwork1.t_system_user (uuid, in_use, mobile, password, username, create_date, delete_date, is_delete, update_date) VALUES ('8ac4d16065418ad10165418addd60000', '0', null, 'e10adc3949ba59abbe56e057f20f883e', 'admin', null, null, '0', null);