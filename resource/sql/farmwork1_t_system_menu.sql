CREATE TABLE farmwork1.t_system_menu
(
    uuid varchar(40) PRIMARY KEY NOT NULL COMMENT '这是注释',
    is_leaf varchar(40),
    level int(11) COMMENT '这是注释',
    menu_icon varchar(40) COMMENT '这是注释',
    menu_name varchar(40) COMMENT '这是注释',
    menu_remark varchar(40),
    menu_url varchar(40) COMMENT '这是注释',
    parent_uuid varchar(40),
    sort_num int(11) COMMENT '这是注释',
    create_date datetime,
    delete_date datetime,
    is_delete varchar(40),
    update_date datetime
);
INSERT INTO farmwork1.t_system_menu (uuid, is_leaf, level, menu_icon, menu_name, menu_remark, menu_url, parent_uuid, sort_num, create_date, delete_date, is_delete, update_date) VALUES ('297e3bc65cceedf1015ccef978b40002', '0', 1, 'glyphicon glyphicon-th-list', '系统功能', null, '', null, 1, null, null, null, null);
INSERT INTO farmwork1.t_system_menu (uuid, is_leaf, level, menu_icon, menu_name, menu_remark, menu_url, parent_uuid, sort_num, create_date, delete_date, is_delete, update_date) VALUES ('4028b0c45ccefbec015ccefe116e0001', '1', 2, 'glyphicon glyphicon-list-alt', '运价查询', null, '', '297e3bc65cceedf1015ccef978b40002', 1, null, null, null, null);
INSERT INTO farmwork1.t_system_menu (uuid, is_leaf, level, menu_icon, menu_name, menu_remark, menu_url, parent_uuid, sort_num, create_date, delete_date, is_delete, update_date) VALUES ('297e3bc65cceedf1015cceff701f0003', '1', 2, 'glyphicon glyphicon-list-alt', '六段查询', null, '', '297e3bc65cceedf1015ccef978b40002', 2, null, null, null, null);
INSERT INTO farmwork1.t_system_menu (uuid, is_leaf, level, menu_icon, menu_name, menu_remark, menu_url, parent_uuid, sort_num, create_date, delete_date, is_delete, update_date) VALUES ('297e3bc65cceedf1015ccf025fb60004', '1', 2, 'glyphicon glyphicon-list-alt', 'FareDisplay', null, '', '297e3bc65cceedf1015ccef978b40002', null, null, null, null, null);
INSERT INTO farmwork1.t_system_menu (uuid, is_leaf, level, menu_icon, menu_name, menu_remark, menu_url, parent_uuid, sort_num, create_date, delete_date, is_delete, update_date) VALUES ('ff8080815c4446f8015c444bda900004', '0', 1, 'fa fa-cog', '系统设置', null, '', null, 5, null, null, null, null);
INSERT INTO farmwork1.t_system_menu (uuid, is_leaf, level, menu_icon, menu_name, menu_remark, menu_url, parent_uuid, sort_num, create_date, delete_date, is_delete, update_date) VALUES ('ff8080815c4464db015c449cf18c0001', '1', 2, 'fa fa-user-secret', '系统用户', null, '/web/user/UserList.html', 'ff8080815c4446f8015c444bda900004', 0, null, null, null, null);
INSERT INTO farmwork1.t_system_menu (uuid, is_leaf, level, menu_icon, menu_name, menu_remark, menu_url, parent_uuid, sort_num, create_date, delete_date, is_delete, update_date) VALUES ('ff8080815ca0f9e0015ca0fe9ec70001', '1', 2, 'fa fa-bullhorn', '系统常量', null, '', 'ff8080815c4446f8015c444bda900004', 1, null, null, null, null);
INSERT INTO farmwork1.t_system_menu (uuid, is_leaf, level, menu_icon, menu_name, menu_remark, menu_url, parent_uuid, sort_num, create_date, delete_date, is_delete, update_date) VALUES ('ff8080815c4464db015c449d15460002', '1', 2, 'fa fa-bars', '系统菜单', null, '/web/menu/MenuList.html', 'ff8080815c4446f8015c444bda900004', 2, null, null, null, null);
INSERT INTO farmwork1.t_system_menu (uuid, is_leaf, level, menu_icon, menu_name, menu_remark, menu_url, parent_uuid, sort_num, create_date, delete_date, is_delete, update_date) VALUES ('8ac4d16064b5adcc0164b6351ee10013', '1', 2, 'glyphicon glyphicon-briefcase', 'FareSearch', null, '', '297e3bc65cceedf1015ccef978b40002', 4, null, null, null, null);
INSERT INTO farmwork1.t_system_menu (uuid, is_leaf, level, menu_icon, menu_name, menu_remark, menu_url, parent_uuid, sort_num, create_date, delete_date, is_delete, update_date) VALUES ('8ac4d16064b637c50164b6480efb000b', '1', 2, 'menu-icon fa fa-tachometer', '一级菜单', null, '', null, 0, null, null, null, null);
INSERT INTO farmwork1.t_system_menu (uuid, is_leaf, level, menu_icon, menu_name, menu_remark, menu_url, parent_uuid, sort_num, create_date, delete_date, is_delete, update_date) VALUES ('8ac4d16064d438220164d4394647000e', '0', 1, 'glyphicon glyphicon-th-list', 'GO', null, '', null, 6, null, null, null, null);